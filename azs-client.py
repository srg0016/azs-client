import time
import urllib.request
import json
import serial
import struct
import threading
import sys
import os
import minimalmodbus

##API_KEY = os.getenv("API_KEY")
#BYTE_LEN = int(os.getenv("BYTE_LEN"))
BYTE_LEN = 5
API_KEY = "2:t5BPWTf/f9UGJQdXc6VAJLVoXSpenfba8vOzhb6SaLpJ"

#port_currents = serial.Serial('/dev/ttyUSB_currents', 9600)
#port_prices = serial.Serial('/dev/ttyUSB_prices', 2400, bytesize = serial.EIGHTBITS, parity = serial.PARITY_EVEN)
#port_currents_fast = serial.Serial('/dev/ttyUSB_currents_fast', 9600)
port_radio = serial.Serial('/dev/ttyUSB_radio', 9600)

instrument = minimalmodbus.Instrument('/dev/ttyUSB_currents', 27)
instrument.serial.baudrate = 9600
instrument.serial.stopbits = 2

currents = [0]*24
prices = [0, 0, 0, 0, 0, 0, 0, 0]
currentsMA = [0]*24
currentTable = [0, 430, 827, 1505, 1852, 2100, 2790]
adc = [0, 66, 167, 340, 432, 514, 692]
channelK = [1, 1.03, 1.02, 0.99, 0.98, 0.99, 1.02, 0.99]
channelOffset = [0, 0, 0, 0, 0, 0, 0, 0]
channelOffsetZero = [0, 0, 0, 0, 0, 0, 0, 0]
currentsRcvFlag = 0
coef = 0.0167

body = {
  "prices": {
    "1": 10,
    "2": 20,
    "3": 30,
    "4": 40,
    "5": 50,
    "6": 60,
    "7": 70,
    "8": 80,
  },
  "currents": {
    "1": 0,
    "2": 0,
    "3": 0,
    "4": 0,
    "5": 0,
    "6": 0,
    "7": 0,
    "8": 0,
    "9": 0,
    "10": 0,
    "11": 0,
    "12": 0,
    "13": 0,
    "14": 0,
    "15": 0,
    "16": 0,
    "17": 0,
    "18": 0,
    "19": 0,
    "20": 0,
    "21": 0,
    "22": 0
  }
}

myurl = "https://stelamonitor.ru/api/v1/data/current"
req = urllib.request.Request(myurl)
req.add_header('Content-Type', 'application/json; charset=utf-8')
req.add_header('Authorization', 'Bearer ' + API_KEY)
jsondata = json.dumps(body)
jsondataasbytes = jsondata.encode('utf-8')   # needs to be bytes
req.add_header('Content-Length', len(jsondataasbytes))
print (jsondataasbytes)

def get_currents(port):
        if (port.inWaiting()> BYTE_LEN*10+2):
                if port.read() == b'\x01':
                        data_str = port.read(port.inWaiting())
                        if data_str[BYTE_LEN*10] == 0x02:
                                xor = 0x01
                                for i in range(BYTE_LEN*10):
                                        xor ^= data_str[i]
                                xor ^= 0x02
                                if xor == data_str[BYTE_LEN*10+1]:
                                        #temp1 = int(data_str[BYTE_LEN*8:BYTE_LEN*(8+1)].decode("utf-8"))*0.03125
                                        #temp2 = int(data_str[BYTE_LEN*9:BYTE_LEN*(9+1)].decode("utf-8"))*0.03125
                                        for i in range(8):
                                                currents[i] = int(data_str[BYTE_LEN*i:BYTE_LEN*(i+1)].decode("utf-8"))
                                                currents[i] = currents[i] + channelOffset[i]
                                                for j in range(len(adc)-1):
                                                        if currents[i] >= adc[j] and currents[i] < adc[j+1]:
                                                                k = (currentTable[j+1] - currentTable[j])/(adc[j+1] - adc[j])
                                                                currentsMA[i] = k*currents[i] + currentTable[j] - k*adc[j]
                                                                currentsMA[i] = currentsMA[i]*channelK[i]
                                                if currents[i] >= adc[len(adc)-1]:
                                                        k = (currentTable[len(adc)-1] - currentTable[len(adc)-2])/(adc[len(adc)-1] - adc[len(adc)-2])
                                                        currentsMA[i] = k*currents[i] + currentTable[len(adc)-1] - k*adc[len(adc)-1]
                                                        currentsMA[i] = currentsMA[i]*channelK[i]
                                                if currents[i] < adc[0]:
                                                        k = (currentTable[1] - currentTable[0])/(adc[1] - adc[0])
                                                        currentsMA[i] = k*(currents[i] + channelOffsetZero[i]) - k*adc[0]
                                                        currentsMA[i] = currentsMA[i]*channelK[i]
                                                body['currents'][str(i+1+16)] = round(abs(currentsMA[i])/1000, 3)
                                        #print(temp1, temp2)
                                        #print(currents)
                                        #print(body)

def get_prices(port):
	if (port.inWaiting() > 14):
		if port.read() == b'\x02':
			packet = port.read(port.inWaiting())
			if packet[12] == 0x03:
				xor_1 = packet[0] 
				for i in range(1, 13):
					xor_1 ^= packet[i]
				print(xor_1)
				if xor_1 == packet[13]:
					price_num = packet[3] & 0x0F
					print(price_num)
					#if price_num == 5:
						#price_num = 1
					#if price_num == 6:
						#price_num = 2
					#if price_num == 7:
						#price_num = 3
					#if price_num == 8:
						#price_num = 4
					price = 10*(packet[8] & 0x0F) + 1*(packet[9] & 0x0F) + 0.1*(packet[10] & 0x0F) + 0.01*(packet[11] & 0x0F)
					priceX = 1000*(packet[8] & 0x0F) + 100*(packet[9] & 0x0F) + 10*(packet[10] & 0x0F) + 1*(packet[11] & 0x0F)
					#print(price)
					if price_num == 1 or price_num == 2 or price_num == 3 or price_num == 4:
						body['prices'][str(price_num)] = price
						prices[price_num-1] = priceX

def get_radio(port):
	N = 99
	if (port.inWaiting() > N-1):
		if port.read() == b'\x01':
			data_str = port.read(port.inWaiting())
			port.write(b'\x01')
			port.write(bytearray(data_str))
			if data_str[N-3] == 0x02:
				xor = 0x01
				for i in range(N-3):
					xor ^= data_str[i]
				xor ^= 0x02
				if xor == data_str[N-2]:
					for i in range(16):
						currents[i] = int(data_str[4*i:4*(i+1)].decode("utf-8"))
						body['currents'][str(i+1)] = (currents[i])/1000
					for i in range(8):
						prices[i] = int(data_str[64+4*i:64+4*(i+1)].decode("utf-8"))
						body['prices'][str(i+1)] = prices[i]/100
					#print(currents)
					#print(prices)

def send_request():
	jsondata = json.dumps(body)
	jsondataasbytes = jsondata.encode('utf-8')
	req.add_header('Content-Length', len(jsondataasbytes))
	response = urllib.request.urlopen(req, jsondataasbytes, timeout = 3)

t0 = time.time()

while True:
        if (time.time() - t0) > 2.0:
                t0 = time.time()
                try:
                        body['currents'][str(17)] = coef*instrument.read_register(5146, number_of_decimals=0, functioncode=4, signed=False)
                        body['currents'][str(18)] = coef*instrument.read_register(5144, number_of_decimals=0, functioncode=4, signed=False)
                        body['currents'][str(19)] = coef*instrument.read_register(5142, number_of_decimals=0, functioncode=4, signed=False)
                        body['currents'][str(20)] = coef*instrument.read_register(9242, number_of_decimals=0, functioncode=4, signed=False)
                        body['currents'][str(21)] = coef*instrument.read_register(9240, number_of_decimals=0, functioncode=4, signed=False)
                        body['currents'][str(22)] = coef*instrument.read_register(9238, number_of_decimals=0, functioncode=4, signed=False)
                except IOError:
                        print("Failed to read from instrument")
                print(body)
                try:
                        send_request()
                #except HTTPError as e:
                #       print(e.response.status_code)
                except:
                        print("Error:", sys.exc_info()[0])
                #port_currents_fast.write(b'G')
                #time.sleep(0.01)
        #get_currents(port_currents_fast)
        #get_currents(port_currents)
        get_radio(port_radio)